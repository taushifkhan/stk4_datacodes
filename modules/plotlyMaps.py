import plotly.io as pio
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
init_notebook_mode(connected=True)
import numpy as np

cDict={ 'cmap2':[[0.0,'rgb(254,232,200)'],[0.0,'rgb(254,232,200)'],
         [0.1,'rgb(166,189,219)'],[0.2,'rgb(166,189,219)'],
         [0.3,'rgb(49,130,189)'],[0.45,'rgb(49,130,189)'],
         [0.5,'rgb(43,140,190)'],[0.6,'rgb(43,140,190)'],[0.7,'rgb(240,59,32)'],[1,'rgb(240,59,32)']],
       'cscale_RYB':[
        [0, 'rgb(44,123,182)'],
        [0.20, 'rgb(44,123,182)'],

        [0.20, 'rgb(158,202,225)'],
        [0.35, 'rgb(158,202,225)'],

        [0.40, 'rgb(255,237,160)'],
        [0.60, 'rgb(255,237,160)'],

        [0.65, 'rgb(252,146,114)'],
        [0.80, 'rgb(252,146,114)'],

        [0.80, 'rgb(215,25,28)'],
        [1, 'rgb(215,25,28)'],
        ]
      }

def plotHeatmap2(v_sub, xnames,ynames, widthv,pretty_scaling=0,\
                 cscale='cmap2',maxP=4, minP=-4, plot_scaling=0,ytickLocation='right'):
    yv_sub = ynames
    subj_col = v_sub.columns +'S'

    width_val=widthv
    height_val = np.max([len(yv_sub)*20, 500])
    if height_val >= 1200:
        height_val = len(yv_sub) * 8

    if pretty_scaling:
        zval = getScaleCorrection_foldChange(v_sub.values,maxP=4,minP=-4)
    else:
        zval = v_sub.values

    if cscale=='cscale_RYB':
        cbar = {'title': 'FoldChange', \
                'tickmode': 'array','len':0.4,'x':-0.5,'xanchor':'center'}
    else:
        cbar = {'title': 'Score', 'titleside': "top", 'tickmode': 'array','tickvals': [0,1,5,10,20],
                          'ticktext':['0','>1','Significant','10','Max',],'ticks':'outside',
                          'len':0.6}

    plotData_ = {
        'data': [
            go.Heatmap(
                z=zval,
                y=v_sub.index,
                x=subj_col,
                xgap=0.2,
                ygap=0.2,
                colorscale=cDict[cscale],
                colorbar=cbar,
            )
        ],
        'layout':
            go.Layout(
            autosize=False,
            width = width_val,
            height= height_val,
            yaxis={'automargin':True,'tickvals':np.arange(len(ynames)),\
                   'ticktext':ynames,'tickfont':{'size':12},'side':ytickLocation},
            #yaxis={'automargin':True,'tickvals':np.arange(len(ynames)),'ticktext':ynames,'tickfont':{'size':12},'side':'right'},
            xaxis={'automargin':True,'tickvals':np.arange(len(subj_col)),'ticktext':xnames,'side':'bottom','tickfont':{'size':8}},
        )
    }
    return plotData_

def getScaleCorrection_foldChange(Df, maxP=4, minP=-4):
    """
    Df : Array of array
    """
    Df = np.array(Df)
    try:
        minv = Df.min()
        maxv = Df.max()
    except:
        minv = 0
        maxv = 15

    print(minv, maxv)
    v_range = maxv - minv
    _v = (((maxP - minP) * (Df - minv)) / v_range) + minP

    # scaling correction. Do not rescale for values of zero and +2 and -2
    Df_zero = np.where(Df == 0)
    Df_l2 = np.where((Df >= -1.5)&(Df < 0))
    Df_g2 = np.where((Df <= 1.5)&(Df > 0))
    _v[Df_zero] = 0
    _v[Df_l2]   =  Df[Df_l2]
    _v[Df_g2]   = Df[Df_g2]

    return _v
