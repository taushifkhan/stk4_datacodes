import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns
import altair as alt

def vsAltPLot(sppScores,bar_height= 60, plot_width=200, plot_height=300):
    cmxB = sns.clustermap(sppScores,figsize=(3,2),cmap='Reds')
    k = cmxB.data2d # fetch the data in clustered 2d
    # estimating significant and detected species
    _vcount = k[k>=1].fillna(0).astype(bool).sum(axis=0).to_frame()
    _vcount = _vcount.rename({0:'spCountPos'},axis=1)
    _vcount['Significant'] = _vcount.spCountPos/k.shape[0]
    _vcount['Detected'] = k[k>0].fillna(0).astype(bool).sum(axis=0).to_frame()/k.shape[0]
    # Unstacking the data for plotting
    _barplotD = _vcount[['Significant','Detected']].unstack().reset_index().rename({'level_0':'RF_Type','level_1':'sid',0:'Spp_RF'},axis=1)
    k2d = k.unstack().reset_index().rename({'level_0':'sampleID',0:'adjSpeciesScore'},axis=1)

    # plot in altair
    # bar plot
    posSpp = alt.Chart(_barplotD,height=bar_height,width=plot_width).mark_bar(opacity=0.7).encode(
        x=alt.X('sid',sort=list(k.columns),axis=alt.Axis(title='SampleID')),
        y=alt.Y('Spp_RF',stack=None),
        color=alt.Color('RF_Type',scale=alt.Scale(range=['Blue','#d9d9d9'],domain=['Significant','Detected']))
        )

    col2dP = alt.Chart(k2d,height=plot_height,width=plot_width).mark_rect().encode(
        x=alt.X('sampleID',sort=list(k.columns),axis=alt.Axis(labels=False,title='')),
        y=alt.Y('Species',sort=list(k.index)),
        color=alt.Color('adjSpeciesScore',scale=alt.Scale(scheme='bluepurple',
                                                          domain=[0.1,1,2,4],paddingOuter=1,
                    range=["#f9f9f9","#f1f1f1","#9ecae1","#756bb1","#de2d26"],type='threshold',
                                                         ),
                    legend=alt.Legend(title='Adj. Sp_Score',titleOrient="top",type='symbol',
                                      gradientLength=150,values=[0,0.1,1,2,4])),
        tooltip=['adjSpeciesScore','sampleID']
        )

    virPlot_altObj = alt.vconcat(col2dP,posSpp)
    return virPlot_altObj
