##  A novel STK4 mutation impairs T cell immunity through dysregulation of cytokine-induced adhesion and chemotaxis genes. ##
* PMID: [34427831](https://pubmed.ncbi.nlm.nih.gov/34427831/)
* DOI : [Here](https://doi.org/10.1007/s10875-021-01115-2)
* Author:Andrea Guennoun§, Salim Bougarn, Taushif Khan, Rafah Mackeh, Mahbuba Rahman, Fatima Al-Ali1, Manar Ata1, Waleed Aamer1, Debra Prosser, Tanwir Habib, Evonne Chin-Smith1, Khawla Al-Darwish, Qian Zhang, Alya Al-Shakaki, Amal Robay, Ronald Crystal, Khalid Fakhro1, Amal Al-Naimi, Eman Al Maslamani, Amjad Tuffaha, Ibrahim Janahi7, Mohammad Janahi, Donald R. Love, Mohammed Yousuf Karim, Bernice Lo, Amel Hassanψ, Mehdi Adeli, Nico Marr1 (ORCID: 0000-0002-1927-7072)

** Repository curator : Taushif Khan [tkhan@sidra.org] **

### Overview ###

* The repository holds data and scripts for the paper (PMID:34427831).
* 30 August 2021

### In this Repository ###

#### Notebooks ####

** Open in ipython-notebook (from default file viewer) from top right corner **

* 1.virScan_viralResponseProfile.ipynb : jupyter notebook for PhipSeq data
* 2.qPCR_ddCTAnnotation.ipynb   : qPCR data analysis qPCR_scripts_29June2021
* 3.RNASeq_responsive_DEG_filtering : script for extracting responsive DEG genes, filtering differentially enriched list from Patient to mean of control. plotting heatmaps and PCA.
* 4.Pathway_STK4roleClueGo.ipynb : scripts for analysising out put from CLUGo pathway analysis curating gene list for STK interactions.

#### curatedData: Directory with experiment specific files ####

* PhipSeqData

1. virscan_skt4Fam_filteredSpScore.csv : Species specific adjusted score for samples.
2. pval_virscan_at1Enriched_annot.csv  : Peptide enrichment profile of samples with filtered list where at-least one sample is enriched.

* qPCRData

1. qPCR_FC_1181.csv : Fold change values of samples with different stimulation condition
2. qpcr_FCvalue_49Genes.csv : Filtered geneset that contributes maximum to first two pincipal component of qPCR fold chnage values among subjects (P to controls)

* RNASeqData

1. LogFC_IFNab_PMA_14247.csv : Log2FC value of DEG genes in Stim/Unstim condition for samples
2. LogCPM_IFNab_PMA_14247.csv : Log CPM values for both stimulation
3. PValue_IFNab_PMA_14247.csv : P-values for each DEG gene.
4. ExpDesigning_IFN_PMA.csv : sample metadata for cohot assignment
5. fc_IFNab_DEG_responsiveSet_1030.csv : Log2FC of responsive genesets among DEGs for IFN alpha
6. fc_PMA_DEG_responsive_3122.csv : Log2FC of responsive genesets among DEGs for PMA stimulation
7. INFab_P_CtrlMean_313_DE_23June2021.csv : Filtered gene set from responsive set for Differential enriched in Patient and mean of controls for IFN stimulation.
8. PMA_P_CtrMean_740_DE_23June2021.csv :Filtered gene set from responsive set for Differential enriched in Patient and mean of controls for PMA stimulation.

* CluGO_pathways [directories]

1. INF_expEvid_0.05 : directory for pathway analysis for 313 IFN stimulated gene
2. PMA_expEvid_0.05 : directory for pathway analysis for 740 IFN stimulated gene
3. STK4_focusedPathway : go_ids_pathwayName_number of DEG.csv

#### modules: Directory containing python scripts used in python notebooks ####

#### datasets: ####
  1. ISG_Shaw2017 : List of human speccific ISG genes from Shaw 2017 (PMID: 29253856)
  2. interactomeDB : STK4 interacting gene set from HuRI PMID: 32296183) and functional protein-protein interactions were verified with string database (v. 11.0b : PMID: 30476243)

#### Figures ####
Directory to save figure from the jupyter notebooks

**package-list.txt** : python library required to generate figures and codes
